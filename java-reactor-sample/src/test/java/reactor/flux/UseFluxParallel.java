package reactor.flux;

import org.junit.Test;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;

public class UseFluxParallel {

    @Test
    public void flux_parallel(){

        // 시퀀스1
        Flux<Integer> seq1 = Flux.range(0, 10).log()
                .map(e -> {
                    sleep(500);
                    return e * e;
                })
                .subscribeOn(Schedulers.parallel());

        // 시퀀스2
        Flux<Integer> seq2 = Flux.range(0, 10).log()
                .map(e -> {
                    sleep(1000);
                    return e * 2;
                })
                .subscribeOn(Schedulers.parallel());

        // 시퀀스3
        Flux<Integer> seq3 = Flux.range(0, 10).log()
                .map(e -> {
                    sleep(300);
                    return e + 100;
                })
                .subscribeOn(Schedulers.parallel());


        Flux.zip(seq1, seq2, seq3).log()
                .subscribe(e -> System.out.println(e), error -> {}, () -> {
                    System.out.println("all complete");
                });

        sleep(11000);
    }

    private void sleep (long m) {
        try {
            Thread.sleep(m);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
