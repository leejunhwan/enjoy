package reactor.flux;

import org.junit.Test;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import reactor.core.publisher.Flux;

import java.util.ArrayList;
import java.util.List;


public class UseFluxWithStream {

    @Test
    public void flux_stream_with_map() {
        List<Integer> elements = new ArrayList<>();

        Flux.just(1, 2, 3, 4)
                .log()
                .map(i -> i * i)
                .subscribe(elements::add);

        elements.stream().forEach(e -> System.out.println(e));
    }

    @Test
    public void flux_combine_two_stream() {
        List<String> elements = new ArrayList<>();

        Flux.just(1, 2, 3, 4)
                .log()
                .map(i -> i * i)
                .zipWith(
                        Flux.just(1, 2, 3, 4),
                        (one, two) -> two + " squared number is "+ one)
                .subscribe(elements::add);

        elements.stream().forEach(e -> System.out.println(e));
    }
}
