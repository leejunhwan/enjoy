package reactor.flux;

import org.junit.Test;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import reactor.core.publisher.Flux;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class UseFluxTest {

    @Test
    public void flux_use_string () {
        List<String> name = new ArrayList<>();

        Flux.just("terry", "john").log().subscribe(e -> {
            if(e.equals("terry")) name.add("테리");
            else if (e.equals("john")) name.add("존");
        }, error -> {

        }, () -> {
            name.stream().forEach(e -> System.out.println(e));
        });
    }


    @Test
    public void flux_use_integer () {
        List<Integer> elements = new ArrayList<>();
        Flux.just(1, 2, 3, 4)
                .log()
                .subscribe(new Subscriber<Integer>() {
                    @Override
                    public void onSubscribe(Subscription s) {
                        s.request(4);
                    }

                    @Override
                    public void onNext(Integer integer) {
                        elements.add(integer);
                    }

                    @Override
                    public void onError(Throwable t) {}

                    @Override
                    public void onComplete() {
                        elements.stream().forEach(e -> System.out.println(e));
                    }
                });
    }


    @Test
    public void flux_use_3_simple () {

        List<Integer> elements = new ArrayList<>();

        Flux.just(1, 2, 3, 4)
                .log()
                .subscribe(
                        elements::add,
                        error -> {},
                        () -> {
                    elements.stream().forEach(e -> System.out.println(e));
                });
    }


    @Test
    public void flux_use_list () {
        List<List<String>> nbaPlayer = new ArrayList<>();
        List<String> pointRank2018 = Arrays.asList(new String[]{"curry", "harden", "durant"});
        List<String> pointRank2019 = Arrays.asList(new String[]{"leonard", "irving", "james"});
        nbaPlayer.add(pointRank2018);
        nbaPlayer.add(pointRank2019);

        Flux.fromIterable(nbaPlayer).log()
                .subscribe(e -> e.stream().forEach( i -> System.out.println(i)));
    }
}
