package com.example.mongodb.curd.repo;

import com.example.mongodb.curd.model.Course;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseRepository extends MongoRepository<Course, Integer>{
    Course findCourseByName(String name);

    Integer deleteByName(String name);
}
