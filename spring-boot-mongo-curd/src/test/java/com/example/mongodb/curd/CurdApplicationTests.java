package com.example.mongodb.curd;

import com.example.mongodb.curd.model.Course;
import com.example.mongodb.curd.repo.CourseRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;


@RunWith(SpringJUnit4ClassRunner.class)
@DataMongoTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CurdApplicationTests {

    @Autowired
    CourseRepository courseRepository;

    String name = "computer";
    String place = "c-building 4F";
    String newPlace = "b-building 1F";

    @Before
    public void mongo_create() {
        Course course = new Course(name,place);
        courseRepository.save(course);
    }

    @Test
    public void a_mongo_select_update() {
        // select
        Course course = courseRepository.findCourseByName(name);
        assertEquals(course.getName(), name);
        assertEquals(course.getPlace(), place);


        // update & select
        course.setPlace(newPlace);
        courseRepository.save(course);

        course = courseRepository.findCourseByName(name);

        assertEquals(course.getName(), name);
        assertEquals(course.getPlace(), newPlace);
    }


    @After
    public void mongo_delete() {
        Course course = courseRepository.findCourseByName(name);

        // delete
        int n = courseRepository.deleteByName(course.getName());
        assertEquals(n, 1);
    }
}
